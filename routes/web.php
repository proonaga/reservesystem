<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect('login');
});

/*
|--------------------------------------------------------------------------
| ユーザー管理
|--------------------------------------------------------------------------
 */
//ログイン/登録
Auth::routes();

//ユーザー編集初期画面
Route::get('users/edit', 'UsersController@edit');
//ユーザー編集処理
Route::post('users/update', 'UsersController@update');

Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| 予約
|--------------------------------------------------------------------------
 */
//初期表示
Route::get('/reserve',              'ReserveController@index')->name('reserve');
// 一覧、検索
Route::get('reserve/search',        'ReserveController@search');
// 更新
Route::get('reserve/edit',          'ReserveController@edit');
Route::get('reserve/edit/{id}',     'ReserveController@edit');
// 登録更新処理
Route::post('reserve/save',         'ReserveController@save');
// 削除
Route::post('reserve/destroy/{id}', 'ReserveController@destroy');
Route::get('reserve/destroy/{id}',  'ReserveController@destroy');

/*
|--------------------------------------------------------------------------
| 開催日設定
|--------------------------------------------------------------------------
 */
// 更新
Route::get('exhibition/edit',       'ExhibitionController@edit');
Route::get('exhibition/edit/{id}',  'ExhibitionController@edit');
// 登録更新処理
Route::post('exhibition/save',      'ExhibitionController@save');
// 開催日付の作成画面
Route::get('exhibition/make',       'ExhibitionController@make');
Route::get('exhibition/make/{id}',  'ExhibitionController@make');
// 開催日付の作成処理
Route::post('exhibition/makesave',  'ExhibitionController@makesave');

/*
|--------------------------------------------------------------------------
| 空き状況
|--------------------------------------------------------------------------
 */
//初期表示
Route::get('/schedule',              'ScheduleController@index')->name('schedule');
// 一覧
Route::get('schedule/search',        'ScheduleController@search');
