<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use SoftDeletes;

    // モデルと関連しているテーブル ※クラス名と不一致の場合に設定する
    protected $table = 'schedule';

    // 主キーとなるカラム
    protected $primaryKey = 'id';

    // 主キーがAutoIncrementであるか
    public $incrementing = true;

    // 主キーとなるカラム
    protected $keyType = 'int';

    // モデルのタイムスタンプを更新するかの指示
    public $timestamps = true;


}
