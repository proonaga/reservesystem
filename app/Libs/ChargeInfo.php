<?php

namespace App\Libs;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

/**
* ログインにて担当者マスタを読み込む。
* 読み込んだ担当者マスタはセッションに保存する。
* 保存したセッション変数へのアクセスには以下のようにアクセスする。
*
* (例：担当者マスタ(ログイン情報)へのアクセス)
*
*/
class ChargeInfo {

    const SESSIONKEY = '___chargeinfo';
    const CONFIG_COMPANY = '___chargeinfo_company';

    public static function load() {

        // 一旦、クリア
        self::release();

        if(Auth::check()){
            // 担当者マスタ
            //todo: モデル使った方がいいが日本語名なので後回し
            $user = Auth::user();
//            $charge = DB::table('担当者マスタ')->where('担当者番号',$user->user_id)->first();
            // $charge = DB::table('charge')
            //             ->where('login_id',$user->id)
            //             ->first();
            $charge = Auth::user();
            // セッションへログイン情報を登録
            session([self::SESSIONKEY=>$charge]);
            // // ログイン情報２
            // $company = DB::table('TABLENAME')->first();
            // session([self::CONFIG_COMPANY=>$company]);
        }else{
            // ログインしていない場合は何もしない
        }

    }
    public static function release(){
        session()->forget(self::SESSIONKEY);
        // session()->forget(self::CONFIG_COMPANY);
    }

    public function __get($name){

        if($name=='user_id'){
            $user = Auth::user();
            return $user->user_id;
        }
        if($name=='user_name'){
            $user = Auth::user();
            return $user->user_name;
        }



        if(Auth::check()){
            // ログインしている場合、該当の情報を検索して返却する
        } else{
            // まだログインされていない場合は何も返さない
            return;
        }
        $sess = session(self::SESSIONKEY);
        if (!empty($sess)){
            if (method_exists($sess, $name)){
                return $sess->$name();
            }elseif(property_exists($sess, $name)){
                return $sess->$name;
            }
            // $sess = session(self::CONFIG_COMPANY);
            // if (!empty($sess)){
            //     if (method_exists($sess, $name)){
            //         return $sess->$name();
            //     }elseif(property_exists($sess, $name)){
            //         return $sess->$name;
            //     }
            // }

            return;
        } else {
            return;
        }
    }


}
