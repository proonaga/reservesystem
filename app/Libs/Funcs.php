<?php
namespace App\Libs;

use Illuminate\Http\Request;
use App\Libs\Helpers;
use DB;

use App\Models\Numbering;
use App\Models\Tax;

class Funcs {

    public static function log($value){
        return logger()->alert(print_r($value,true));
    }
    // 税額
    public static function calctax($amount, $taxrate=-1) {
        if($taxrate < 0){
            $taxrate = Tax::rate();
        }
        switch(config('const.tax.current')) {
            case config('const.tax.mathtype.floor'): //切り捨て
                return floor($amount / 100 * self::val($taxrate));
                break;
            case config('const.tax.mathtype.ceil'): //切り捨て
                return ceil($amount / 100 * self::val($taxrate));
                break;
            case config('const.tax.mathtype.round'): //切り捨て
                return round($amount / 100 * self::val($taxrate));
                break;
        }
    }
    // 金額計算（税込）
    public static function calcamount($price, $quantity=1, $taxrate=-1) {
        $amount = self::val($price)*self::val($quantity);
        $tax = self::calctax($amount, $taxrate);
        return $amount + $tax;
    }

    /**
    * 採番マスタから新規IDを取得する
    */
    public static function nextid($key=false){
        // 戻り値
        $result = 0; //Empty「0」で初期化
        // ロックをかける為、トランザクションを開始
        DB::beginTransaction();
        try {
            // KEY取得
            $keys = config('const.nextidkeys');
            if(empty($key)){
                $key = $keys['sales'];
            }
            // ロックをかける
            $ret = Numbering::where('numbering_key',$key)
                            ->lockForUpdate()
                            ->get()
                            ;
            // KEYに該当するレコードを検索
            $ret = Numbering::where('numbering_key',$key)
                            ->increment('current_id')
                            ;
            // 加算後の値を取得
            $ret = Numbering::where('numbering_key',$key)
                            ->select('current_id')->first()
                            ;
            // 戻り値にセット
            $result = $ret['current_id'];
            // コミットでロック解除
            DB::commit();
        } catch (\PDOException $e){
            DB::rollBack();
        }
        // 戻り値
        return $result;
    }

    /**
    * 初期値付きで変数から値を取り出す
    */
    public static function nvl($value, $default = false){
        if(empty($value)){
            return $default;
        }
        return $value;
    }

    /**
    * FORMリクエストの入力値取得
    * @todo 値がクリアされた場合の確認
    */
    public static function requested($name, $default = false){
        if(!empty(old($name))){
            return old($name);
        }elseif(array_key_exists($name,$_REQUEST) && !empty($_REQUEST[$name])){
            return $_REQUEST[$name];
        }
        return $default;
    }

    /**
    * 該当のKEYから値を取り出す
    *
    * @param object 変換する値
    * @return integer
    */
    public static function vl($key, $object, $default=0){
        if (is_array($object)){
            if (array_key_exists($key, $object)) {
                return $object[$key];
            }
        }
        if (is_object($object)){
            if (method_exists($object, $key)){
                return $object->$key();
            }elseif (property_exists($object, $key)){
                return $object->$key;
            }elseif(is_subclass_of($object,'FormRequest')){
                return $object[$key];
            }elseif(is_subclass_of($object,'Illuminate\Foundation\Http\FormRequest')){
                return $object[$key];
            }
        }
        if (function_exists($key)){
            return $key();
        }
        // 確認１
        $res = @$object->$key;
        if (!empty($res)){
            return $res;
        }
        return $default;
    }

    /**
    * FORMリクエストの入力値取得
    * 1. リクエストに、$name が含まれる -> リクエストから値を戻す
    * 2. リクエストに該当せず、$loadedに$nameが含まれる -> $loadedから値を返す
    * 3. リクエストおよび$loadedに該当しない場合 -> $defaultを返す
    *
    * @param $name Requestの名前
    * @param $loaded
    * @param $default
    * @return mixed
    *
    * @todo 値がクリアされた場合の確認
    */
    public static function rq($name, $loaded = false, $default = false){
        return Funcs::nvl(Funcs::requested($name, Funcs::vl($name, $loaded)),$default);
    }

    /**
    * 値をIntegerで返却
    * （E_NOTICEを出さないようにした intval()関数 ）
    *
    * @param mixed 変換する値
    * @return integer
    */
    public static function iv($value){
        if (!is_object($value)){
            return intval($value);
        }
        return 0;
    }

    /**
    * 値をFLOATで返却
    *
    * @param mixed 変換する値
    * @return integer
    */
    public static function val($value){
        if (!is_object($value)){
            return floatval($value);
        }
        return 0;
    }

    /**
    * 値を日付で返却
    *
    * @param mixed 変換する値
    * @return string
    */
    public static function d($value, $format = 'Y/m/d', $default_is_today=false){
        if(empty($value) && $default_is_today===true){
            return date($format);
        }elseif(empty($value) && $default_is_today===false){
            return;
        }

        if($default_is_today===true){
            $default = date($format);
        }elseif( $default_is_today===false){
            $default = false;
        }else{
            $default = $default_is_today;
        }

        if (empty($value)) return $default;

        return date($format, strtotime($value));
    }

}
