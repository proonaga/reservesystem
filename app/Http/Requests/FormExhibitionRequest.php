<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FormExhibitionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            //開催時間
            'exh_time.*'        => '',

            //最大プレート数
            'plate_maxnum.*'    => 'required_with:exh_time.*|max:999|min:0|integer',
        ];
    }

    public static function messages()
    {
        return [
            'exh_time.*.required_unless'      => '最大プレート数が入力されています。:attributeを選択してください。',
            'plate_maxnum.*.required_with'  => '開催時間が選択されています。:attributeを入力してください。',
        ];
    }


}
