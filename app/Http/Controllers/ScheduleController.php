<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests\FormReserveRequest;
use App\Libs\Funcs;
use App\Libs\ChargeInfo;
use App\Models\Reserve;
use App\Models\Schedule;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;


class ScheduleController extends Controller
{

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return $this->search();
//        $results = $this->getData("search");
        return view('schedule.index');
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        //スケジュールデータの取得
        $scheduleData = $this->getData();

        //カレンダー用にデータ整形
        foreach ($scheduleData as $schedule) {
            $exh_date       = $schedule->exh_date;      //開催日
            $exh_time       = $schedule->exh_time;      //開催時間
            $exh_maxnum     = $schedule->plate_maxnum;  //最大プレート人数
            $number         = $schedule->number;        //予約プレート数

            //定員(開催最大人数) > 予約者総数
            //空き状況を背景色で表示
            if ( $exh_maxnum <= $number ) {
                $color = '#FF0000';
            }elseif ( ($exh_maxnum - $number) <= 10 ) {
                //残りわずかです。
                $color = '#ffd700';
//            }elseif ( ($exh_maxnum - $number) <= 30 ) {
//                //空席が残り30名以下でございます。
//                $color = '#3cb371';
            }else {
                //予約OK。
                $color = '#4169e1';
            }

            $results[] = [
                'title'         => date_format( date_create($exh_time), 'H:i'),
                'description'   => date_format( date_create($exh_date), 'Y/m/d'),
                'start'         => date_format( date_create($exh_date), 'Y/m/d'),
                'end'           => $schedule->exh_date,
                'color'         => $color,
                'textColor'     => '#FFFFFF'
            ];
        }

        return $results;
    }
    /**
     * SQLの生成
     * スケジュールデータの取得
     * @return Response
     */
	public function getData()
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        $std = new Carbon(Funcs::vl('startDate', $prms));
        $end = new Carbon(Funcs::vl('endDate',   $prms));
        $start_date = $std->format($std);
        $end_date   = $end->format($end);

        /* ------------------------------------------------------
         * データの取得
         *------------------------------------------------------ */
        $query = DB::table('schedule');
        $query->where('exh_date','>=', $start_date);
        $query->where('exh_date','<=', $end_date);
        $query->whereNotNull('exh_time');
        $query->where('plate_maxnum','>', 0);
        $query->orderBy('exh_date','asc');
        $query->orderBy('exh_time','asc');

        $data = $query->get();

        return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        $data = [];
        $data['results'] = [];

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/

            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $results = Category::find( $id );
                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '分類情報が存在しませんでした。');
                    return redirect('reserve'); //一覧へ戻す
                }

                $data['results']['id']                = $results['id'];                     //ID
                $data['results']['reserve_code']     = $results['reserve_code'];          //分類コード
                $data['results']['reserve_name_fst'] = $results['reserve_name_fst'];      //大分類名
                $data['results']['reserve_name_snd'] = $results['reserve_name_snd'];      //中分類名
                // </editor-fold>
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('reserve.edit', compact('data'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, CategorySaveRequest::rules(), CategorySaveRequest::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            return view('reserve.edit', compact('data'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Category::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 4.1 新規
             * ----------------------------------------*/
            $table = new Category();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 4.2 更新
             * ----------------------------------------*/
            $table = Category::find($id);
            $edit_message = "更新";
        }
        //入力項目のセット
        $table->reserve_code 	  = $request['reserve_code']; 		// 分類コード
        $table->reserve_name_fst = $request['reserve_name_fst']; 	// 大分類名
        $table->reserve_name_snd = $request['reserve_name_snd'];  // 中分類名
        $table->upd_user_name     = $charge->user_name;             // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('reserve');
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = Category::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('reserve');
    }
}
