<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests\FormReserveRequest;
use App\Libs\Funcs;
use App\Libs\ChargeInfo;
use App\Models\Reserve;
use App\Models\Item;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;


class ReserveController extends Controller
{

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        $prms = Request::all();

        /* ------------------------------------------------------
         * データ取得
         *------------------------------------------------------ */
        $results = $this->getData("search");

        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('reserve.index', compact('results'));
    }
    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($get_mode)
	{
        /* ------------------------------------------------------
         * 初期設定
         *------------------------------------------------------ */
        // リクエスト取得
        $prms = Request::all();
		$data['results'] = null;

        /* ------------------------------------------------------
         * 一覧データの取得
         *------------------------------------------------------ */
        // <editor-fold defaultstate="collapsed" desc=" クエリ">
        $query = Reserve::select(
                              'reserve.id'                  //ID
                             ,'reserve.reserve_date'        //予約日
                             ,'reserve.reserve_time'        //予約時間
                             ,'reserve.number_adult'        //大人人数
                             ,'reserve.number_child'        //子供人数
                             ,'reserve.represent_name'      //代表者名
                             ,'reserve.price'               //金額
                             ,'reserve.group_name'          //グループ名
                             ,'reserve.tel'                 //電話番号
                             ,'reserve.email'               //EMAIL
                             ,'reserve.nationality'         //国籍
                             ,'reserve.settlement_type'     //決済方法
                             ,'reserve.comment'             //コメント
//                             ,'reserve_detail.reserve_id'   //予約ID
//                             ,'reserve_detail.item_id'      //商品ID
//                             ,'reserve_detail.plate_num'    //プレート数
//                             ,'item.item_name'              //商品名
//                             ,'item.item_fee'               //料金
                    );
//		$query->join('reserve_detail', 'reserve.id', '=', 'reserve_detail.reserve_id');
//		$query->join('item', 'reserve_detail.item_id', '=', 'item.id');

        //代表者名
        if(!empty(Funcs::vl('represent_name', $prms))){
            $query->where('represent_name','like','%'.Funcs::vl('represent_name', $prms).'%');
        }
        //予約日(開催日)
        if(!empty(Funcs::vl('reserve_date', $prms))){
            $query->where('reserve_date','=',Funcs::vl('reserve_date', $prms));
        }
        //予約時間(開催時間)
        if(!empty(Funcs::vl('reserve_time', $prms))){
            $query->where('reserve_time','=',Funcs::vl('reserve_time', $prms));
        }

        $query->orderBy('reserve_date','desc');
        $query->orderBy('id','desc');
        // </editor-fold>

        /* ------------------------------------------------------
         * 取得タイプ
         * search   : 検索結果取得処理
         * csvprint : エクセルデータ取得処理
         *------------------------------------------------------ */
        if ($get_mode=="search") {
            $data = $query->paginate(Config::get('const.default_maxrowsize'))->appends($prms);
        }else{
            $data = $query->get();
        }

        return $data;
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        /*-----------------------------------------
         * 1. 初期化
         *----------------------------------------- */
        $data = [];
        $data['results'] = [];

        /*-----------------------------------------
         * 2. 登録更新データの取得
         *----------------------------------------- */
        // <editor-fold defaultstate="collapsed" desc=" 登録更新データの取得">
        $hasold = !empty(Input::old());
        if(!$hasold){
            if (empty($id))
            {
                /*---------------------------
                 * 2.1 新規
                 *---------------------------*/
                $item_list = DB::table('item')->get();
                if($item_list->isEmpty()){
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '商品情報が存在しませんでした。商品を登録してください。');
                    return redirect('item'); //商品マスタへ
                }
                $k = 1;
                foreach($item_list as $row) {
                    for ($i=0;$i<=60 ;$i++) {
                        //配列へ格納する
                        $item_lists['items['.$k.']'][$i] = $i.'プレート('.$row->item_name.')';                //開催ID
                    }
                    $k++;
                }
                $da1 = $item_lists['items[1]'];
                $da2 = $item_lists['items[2]'];
            }
            else
            {
                /*---------------------------
                 * 2.2 修正
                 *---------------------------*/
                /* ===========================
                 * 2.2.1 初期表示データの取得
                 * =========================== */
                // <editor-fold defaultstate="collapsed" desc=" 2.2.1 初期表示データの取得">
                $results = Category::find( $id );
                if(empty( $results )) {
                    // 該当がない場合、エラーにして一覧へ戻す
                    Session::flash('flash_danger', '分類情報が存在しませんでした。');
                    return redirect('reserve'); //一覧へ戻す
                }

                $data['results']['id']               = $results['id'];                    //ID
                $data['results']['reserve_code']     = $results['reserve_code'];          //分類コード
                $data['results']['reserve_name_fst'] = $results['reserve_name_fst'];      //大分類名
                $data['results']['reserve_name_snd'] = $results['reserve_name_snd'];      //中分類名
                // </editor-fold>
            }

        }
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return view('reserve.edit', compact('data', 'item_lists'));

    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $id      = Funcs::rq('id', $request);

        // 結果格納用
        $data['results'] = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormReserveRequest::rules(), FormReserveRequest::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            return view('reserve.edit', compact('data'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">
        //存在チェック
        $isnew = empty(Category::find($id));
        if( $isnew ){
            /* ----------------------------------------
             * 4.1 新規
             * ----------------------------------------*/
            $table = new Category();
            $table->add_user_name = $charge->user_name;    // 登録者
            $edit_message = "登録";
        }else{
            /* ----------------------------------------
             * 4.2 更新
             * ----------------------------------------*/
            $table = Category::find($id);
            $edit_message = "更新";
        }
        //入力項目のセット
        $table->reserve_code 	  = $request['reserve_code']; 		// 分類コード
        $table->reserve_name_fst = $request['reserve_name_fst']; 	// 大分類名
        $table->reserve_name_snd = $request['reserve_name_snd'];  // 中分類名
        $table->upd_user_name     = $charge->user_name;             // 更新者

        // 登録更新処理
        $ret = $table->save();

        // 正常終了メッセージ
        Session::flash('flash_success', $edit_message."が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------
        return redirect('reserve');
    }

    /**
     * 論理削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // ログイン情報
        $charge = new ChargeInfo;

        if(!empty($id)){
            $data = Category::find($id);
            if(empty($data)) {
                Session::flash('flash_danger', '削除情報が存在しませんでした。');
            }else{
                $data->delete();                            // 論理削除処理
                $data->upd_user_name = $charge->user_name;  // 更新者
                $data->save();

                // 正常終了メッセージ
                Session::flash('flash_success', "削除が完了しました。");
            }
		}

        // 一覧に戻す
        return redirect('reserve');
    }
}
