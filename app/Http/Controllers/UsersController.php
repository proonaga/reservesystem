<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use Validator;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // 更新用フォーム画面へ移動
    public function edit()
    {
        $user_info['id']        = Auth::user()->id;
        $user_info['user_id']   = Auth::user()->user_id;
        $user_info['user_name'] = Auth::user()->user_name;

        return view('auth.edit', compact('user_info'));
    }

    // 更新処理
    public function update(Request $request)
    {
        $user_info['id']                    = $request->id;
        $user_info['user_id']               = $request->user_id;
        $user_info['user_name']             = $request->user_name;
        $user_info['password']              = $request->password;
        $user_info['password_confirmation'] = $request->password_confirmation;

        //バリデーション実行
        $validator = Validator::make($request->all(), $this->validateRules, $this->validateMsg);
        if ($validator->fails()) {
            return view('auth.edit')->with(compact('user_info'))->withErrors($validator);
        }

        //入力画面へ遷移
        if (Input::get('back')) {
            return view('auth.edit', compact('user_info'));
        }

        //確認画面へ遷移
        if (Input::get('confirm')) {
            $user_info['confirm'] = true;
            return view('auth.edit', compact('user_info'));
        }

        //ID情報の取得
        $user = \App\User::find($request->id);
        if (is_null($user)) {
            //メッセージ表示
            \Session::flash('flash_message', '既に削除されている可能性があります。');
            return redirect()->route('users.index');
        }

        //更新処理
        $user->user_name = $request->user_name;
        if(!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return view('top');
//        return redirect('estimates');
    }

    /**
     * バリデーションルール
     *
     * @return Response
     */
    public $validateRules = [
        'user_name'             => 'required',
        'password'              => 'confirmed|string|nullable|min:6|alpha_num_check',
        'password_confirmation' => 'string|nullable|min:6|alpha_num_check',
    ];

    /**
     * バリデーションメッセージ
     *
     * @return Response
     */
    public $validateMsg = [
        'password.alpha_num_check'              => ':attributeは半角英数字で入力してください。',
        'password_confirmation.alpha_num_check' => ':attributeは半角英数字で入力してください。',
    ];

}
