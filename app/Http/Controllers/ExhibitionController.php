<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests\FormExhibitionRequest;
use App\Libs\Funcs;
use App\Libs\ChargeInfo;
use App\Models\Reserve;
use App\Models\Exhibition;

use Config;
use Carbon\Carbon;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validator;


class ExhibitionController extends Controller
{

    /**
     * コンストラクタ
     */
    public function __construct()
    {
    }

    /**
     * 初期表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return $this->search();
    }

    /**
     * 検索
     *
     * @return Response
     */
    public function search()
    {
    }
    /**
     * SQLの生成
     * 一覧データの取得
     * @return Response
     */
	public function getData($get_mode)
	{
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id=null)
    {
        /*------------------------------------------------------
         * 1. 初期化
         *------------------------------------------------------ */
        $data    = [];
        $results = [];
        $dt      = new Carbon();
        $charge  = new ChargeInfo;
        $prms    = Request::all();

        // 開催時間プルダウン
        $time_list = Config::get('const.time_list');
        $add = array('' => '開催無し',);
        $time_list = array_merge($add, $time_list);

        /*------------------------------------------------------
         * 2. 登録更新データの取得
         *------------------------------------------------------ */
        /*----------------------------------
         * 2.1 引数のセット
         *----------------------------------*/
        // <editor-fold defaultstate="collapsed" desc="2.1 引数のセット">
        if (empty(Funcs::vl('exh_month', $prms)))
        {
            // 2.1.1 当月の格納
            $prm_exh_date = $dt->format('Y/m');
        }
        else
        {
            // 2.1.2 過去/未来月の取得
            $prm_exh_date = Funcs::vl('exh_month', $prms);
        }
        // </editor-fold>

        /*----------------------------------
         * 2.2 開催月データ(画面表示月)の取得
         *----------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 2.2 開催月データ(画面表示月)の取得">

        //SQL：データ取得
        $results     = $this->getExhibition($prm_exh_date);

        if($results->isEmpty()){
            // 該当がない場合、エラーにして一覧へ戻す
            Session::flash('flash_danger', '開催年月日の初期データが存在しませんでした。設定年を作成してください。');
            return view('exhibition.make'); // 「開催日時初期データ作成」へ遷移する。
        }
        // 取得データの総件数
        $cnt_results = count($results);

        // </editor-fold>

        /*----------------------------------
         * 2.3 取得データを配列に入れなおす
         *----------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 2.3 取得データを配列に入れなおす">
        $i = 0;//行番号カウンタ
        $cond_date = new Carbon($prm_exh_date.'/01');
        $chk_flg = false;

        foreach($results as $row){
            // 開催時間の格納
            $ex_dt      = new Carbon($row->exh_date);
            // 比較用：開催時間の格納
            $chk_ex_dt  = $row->exh_date;

            /* ループ内で日付が変わったら、チェックボックスへチェックを入れるか判別する
             * 「開催無し」がある場合      ：$chk_flg  = false;
             * 「開催無し」が一つもない場合：$chk_flg  = true;
             */
            // <editor-fold defaultstate="collapsed" desc=" 2.3.1 チェックボックスへチェックを入れるか判別">
            if ( $chk_ex_dt != $cond_date) {
                $chk_name = substr($cond_date, 8, 2);
                $chk_name = (int)$chk_name;
                if ($chk_flg) {
                    $data[sprintf('chk_ext[%d]', $chk_name)] = '0';   //チェックを入れない
                }else{
                    $data[sprintf('chk_ext[%d]', $chk_name)] = '1';   //チェックを入れる
                }
                $chk_flg  = false;  //初期化：チェックを入れる(false)
            }

            //開催時間
            //NULLの場合:NULLをセット
            //NULL以外  :開催時間をセット
            if ($row->exh_time == null) {
                $ex_t = $row->exh_time;
            }else{
                $ex_t = new Carbon($row->exh_time);
                $ex_t = $ex_t->format('H:s');
                $chk_flg  = true;
            }
            // </editor-fold>

            //配列へ格納する
            $data[sprintf('exh_id[%d]',          $i)] = $row->id;                //開催ID
            $data[sprintf('exh_date[%d]',        $i)] = $ex_dt->format('Y/m/d'); //開催日付
            $data[sprintf('exh_time[%d]',        $i)] = $ex_t;                   //開催時間
            $data[sprintf('plate_maxnum[%d]',    $i)] = $row->plate_maxnum;      //最大プレート数

            $i++;
            $cond_date = $chk_ex_dt;
        }
        //最終行のチェックボックスのチェック有無格納
        $chk_name = substr($cond_date, 8, 2);
        $chk_name = (int)$chk_name;
        $data[sprintf('chk_ext[%d]', $chk_name)] = ($chk_flg)?'0':'1';   //開催時間
        // </editor-fold>

        /*-----------------------------------
         * 2.4 最大イベント数の取得
         *      (VIEW表示時に列の作成にて使用)
         *-----------------------------------*/
        $query = Exhibition::select(
                        'exh_date'
                        ,DB::raw('COUNT(exh_date) AS max_count')
        );
        $query->whereYear('exh_date', substr($prm_exh_date, 0, 4));
        $query->whereMonth('exh_date', substr($prm_exh_date, 5, 2));
        $query->groupBy('exh_date');
        $query->orderBy('exh_date','asc');
        $count     = $query->first();
        $max_count = $count->max_count; //最大イベント数(コマ数)
        // </editor-fold>

        //----------------------------------------------
        // VIEW
        // data       :登録更新データ
        // max_count  :一日の最大イベント数
        // cnt_results:登録更新データ件数
        // time_list  :開催時間プルダウン
        //----------------------------------------------
        return view('exhibition.edit', compact('data', 'max_count', 'cnt_results', 'time_list'));

    }

    /**
     * 開催日付を取得
     *
     * @return Response
     */
    public function getExhibition($prm_exh_date) {

        $query = Exhibition::select('*');
        $query->whereYear('exh_date', substr($prm_exh_date, 0, 4));
        $query->whereMonth('exh_date', substr($prm_exh_date, 5, 2));
        $query->orderBy('exh_date','asc');
        $query->orderBy(DB::raw("exh_time IS NULL"),'asc');
        $query->orderBy('exh_time','asc');

        $data = $query->get();

        return $data;
    }

    /**
     * 登録更新処理
     *
     * @return Response
     */
    public function save()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        // ログイン情報
        $charge  = new ChargeInfo;

        /* ------------------------------------------------------
         * 2.Request
         *------------------------------------------------------ */
        // 2.1 開催日データ項目
        $request = Request::all();
        $exh_month          = Funcs::vl('exh_month',        $request);  //開催月
        $user_id            = $charge->user_id;                         //ユーザーID
        $max_count          = Funcs::vl('max_count',        $request);  //最大イベント数
        $cnt_results        = Funcs::vl('cnt_results',      $request);  //登録更新データ件数

        // 2.2 開催日データ配列項目
        $arr_exh_id         = Funcs::vl('exh_id',           $request);  //開催ID
        $arr_exh_date       = Funcs::vl('exh_date',         $request);  //開催日付
        $arr_exh_time       = Funcs::vl('exh_time',         $request);  //開催時間
        $arr_plate_maxnum   = Funcs::vl('plate_maxnum',     $request);  //最大プレート数

        // 結果格納用
        $data = array();

        /* ----------------------------------------------
         * 2. バリデーション
         * ----------------------------------------------*/
        $validator = Validator::make( $request, FormExhibitionRequest::rules(), FormExhibitionRequest::messages() );

        // バリデーションエラーだった場合
        if ( $validator->fails() ) {

            // 開催時間プルダウン
            $time_list = Config::get('const.time_list');
//            array_unshift($time_list, "開催無し");
            $add = array('' => '開催無し',);
            $time_list = array_merge($add, $time_list);

            // 明細データ
            $i = 0;//行番号カウンタ
            foreach($arr_exh_id as $key=>$exh_id){
                $data[sprintf('exh_id[%d]',         $i)] = $arr_exh_id[$i];          //開催ID
                $data[sprintf('exh_date[%d]',       $i)] = $arr_exh_date[$i]; //開催日付
                $data[sprintf('exh_time[%d]',       $i)] = $arr_exh_time[$i];    //開催時間
                $data[sprintf('plate_maxnum[%d]',   $i)] = $arr_plate_maxnum[$i];      //最大プレート数
                $i++;
            }

            /* End------- Viewへセット ------  */

            return view('exhibition.edit', compact('data', 'max_count', 'cnt_results', 'time_list'))->withErrors($validator);
        }

        /* ----------------------------------------------
         * 3. DB登録
         * ----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. DB登録">

        //明細行をループ
        $cnt = count($arr_exh_id);
        for ($i=0; $i<$cnt ;$i++) {

            /* 新規登録 (ID:存在しない & 開催時間:存在する場合) */
            if ( empty($arr_exh_id[$i]) && !empty($arr_exh_time[$i]) ) {
                /* ----------------------------------------
                 * 3.1 ExhibitionテーブルへINSERT
                 * ----------------------------------------*/
                $table = new Exhibition();
                $table->exh_date        = $arr_exh_date[$i];
                $table->exh_time        = $arr_exh_time[$i];
                $table->plate_maxnum    = $arr_plate_maxnum[$i];
                $table->add_user_name   = $user_id;
                $table->upd_user_name   = $user_id;

                $ret = $table->save();

            /* 更新 (ID存在する場合) or アクション無し */
            }else{
                /* ----------------------------------------
                 * 3.2 ExhibitionテーブルへUPDATE
                 * ----------------------------------------*/
                if ( !empty($arr_exh_id[$i]) ) {
                    $table = Exhibition::where('id','=',$arr_exh_id[$i])->first();
                    $table->exh_time        = ($arr_exh_time[$i] != 0)? $arr_exh_time[$i]: null;
                    $table->plate_maxnum    = $arr_plate_maxnum[$i];
                    $table->upd_user_name   = $user_id;

                    $ret = $table->save();
                }
            }
        }

        // 正常終了メッセージ
        Session::flash('flash_success', "登録が完了しました。");
        // </editor-fold>

        //---------------------------
        // VIEW
        //---------------------------exh_month
        return redirect('exhibition/edit?exh_month='.$exh_month);
    }

    /**
     * 登録更新画面へ遷移
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function make($id=null)
    {
        /* ---------------------------
         * VIEW
         *--------------------------- */
        return view('exhibition.make');
    }

    /**
     * 開催日付初期データの作成
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function makesave()
    {
        /* -----------------------------------------------
         * 1. 初期設定
         * -----------------------------------------------*/
        $charge  = new ChargeInfo;

        // リクエスト
        $request = Request::all();
        $year    = Funcs::rq('maake_date', $request);
        $year    = '2018';

        /* -----------------------------------------------
         * 2. 初期設定
         * -----------------------------------------------*/
        $results = Exhibition::whereYear('exh_date', $year)->get();
        if(!$results->isEmpty()){
            Session::flash('flash_danger', '既に「'.$year.'年」データは初期登録されています。');

            // 一覧に戻す
            return redirect('exhibition/make');
        }

        /* -----------------------------------------------
         * 3. 開催日付データの作成
         * -----------------------------------------------*/
        // <editor-fold defaultstate="collapsed" desc=" 3. 開催日付データの作成">
        // 12か月分ループ
        for ( $i=1; $i <= 12; $i++) {
            /* -------------------------------------
             * 3.1 データ作成月を作成
             * -------------------------------------*/
            $make_date   = $year.'-'.$i;
            $dt          = new Carbon($make_date);
            // データ登録用にフォーマット格納
            $format_date = $dt->format('Y/m/');

            /* -------------------------------------
             * 3.2 該当月の日数分ループ
             * -------------------------------------*/
            // 該当月の日数を取得
            $daysInMonth = $dt->daysInMonth . "\n";
            // 該当月の日数分ループ
            for ( $k=1; $k <= $daysInMonth; $k++) {

                // イベント数分データ登録「10:00,13:00,16:00」
                for ( $m=3; $m <= 9; $m+=3) {
                    $time = 7;
                    $table = new Exhibition();
                    $table->exh_date        = $format_date.$k;
                    $table->exh_time        = $time+$m.':00:00';
                    $table->plate_maxnum    = Config::get('const.default_maxplate');
                    $table->add_user_name 	= $charge->user_id;
                    $table->upd_user_name 	= $charge->user_id;

                    // 登録更新処理実行
                    $ret = $table->save();
                }
            }

        }
        // </editor-fold>

        // 正常終了メッセージ
        Session::flash('flash_success', $year."年初期データ登録完了しました。");

        // 一覧に戻す
        return redirect('exhibition/make');
    }
}
