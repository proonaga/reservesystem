{{-- フラッシュメッセージの表示 --}}
@if (Session::has('flash_success'))
    <div class="alert alert-success" role="alert">{{ Session::get('flash_success') }}</div>
@endif
@if (Session::has('flash_info'))
    <div class="alert alert-info" role="alert">{{ Session::get('flash_info') }}</div>
@endif
@if (Session::has('flash_warning'))
    <div class="alert alert-warning" role="alert">{{ Session::get('flash_warning') }}</div>
@endif
@if (Session::has('flash_danger'))
    <div class="alert alert-danger" role="alert">{{ Session::get('flash_danger') }}</div>
@endif

{{-- エラーメッセージ：ページ上部 表示:ON 非表示:OFF --}}
@if ($err_message_flg == 'ON')
    {{-- フォームエラー --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endif
