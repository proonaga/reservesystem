{{--
    モーダルウィンドウ用の共通コンポーネント

    #---------------------------------------
    #　（呼び出し元の記載例）
    #---------------------------------------
    # @php
    # $modalid = 'modalwindow';
    # @endphp
    # <!-- 切り替えボタンの設定 -->
    # <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#{{$modalid}}">
    #     ココを押すと表示
    # </button>
    # @component('layouts.modal')
    #     @slot('modalid', $modalid)
    #     @slot('modalsize', 0)
    #     @slot('modaltitle', 'Modal test.')
    #     @slot('modalcontent')
    #         <div class="text-muted">ここにダイアログの本文を書きます。</div>
    #         <div class="text-muted">大きいサイズのモーダルは、SLOT'modalsize'に「1」、小さいサイズは「2」を設定します</div>
    #     @endslot
    #     @slot('modalfooter')
    #         <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
    #     @endslot
    #     @slot('modalevents')
    #         $('#{{$modalid}}').on('show.bs.modal', function () {
    #             // モーダルを開く前のイベント
    #         })
    #         $('#{{$modalid}}').on('shown.bs.modal', function () {
    #             // モーダルを開いた後のイベント
    #         })
    #         $('#{{$modalid}}').on('hide.bs.modal', function () {
    #             // モーダルを閉じる前のイベント
    #         })
    #         $('#{{$modalid}}').on('hidden.bs.modal', function () {
    #             // モーダルを閉じた後のイベント
    #         })
    #     @endslot
    # @endcomponent
--}}
<!-- モーダルの設定 -->
<div class="modal fade" id="{{$modalid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?php
    $sizeoption = '';
    if(!isset($modalsize)){
        // 指定なし
    }elseif($modalsize==1){
        $sizeoption = ' modal-lg';
    }elseif($modalsize==2){
        $sizeoption = ' modal-sm';
    }
    ?>
    <div class="modal-dialog{{$sizeoption}}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if(!empty($modaltitle))
                <h5 class="modal-title">{{$modaltitle}}</h5>
                @endif
                <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if(!empty($modalcontent))
            <div class="modal-body">
                {{$modalcontent}}
            </div>
            @endif
            @if(!empty($modalfooter))
            <div class="modal-footer">
                {{$modalfooter}}
            </div><!-- /.modal-footer -->
            @endif
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@if(!empty($modalevents))
{{$modalevents}}
@endif
