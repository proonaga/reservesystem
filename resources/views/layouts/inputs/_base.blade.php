<?php
    $haserror = '';
    if(!empty($errors->first($accesskey))){
        $haserror = 'has-error';
    }
    if(!isset($unerror)) $unerror = false;
?>
<div class="{{ $classes }} {{ $haserror }}">
    {{ $domcontent }}
</div>
@if(!empty($errors->first($accesskey)) && !$unerror)
<div class="alert-err inputerror">
    <ul>
        @foreach ($errors->get($accesskey) as $message)
            <li>{{ $message }}</li>
        @endforeach
    </ul>
</div>
@endif
