<?php
    /**
    * @slot accesskey ID、Nameに利用される（必須）
    * @slot pulldown プルダウンリスト（必須）
    * @slot classes input-groupなどを指定。<DIV>に適用される
    * @slot document <INPUT>タグ
    * @slot loading VIEWに引き渡された値。Input::oldや$dataなど
    * @slog default <INPUT VALUE>のデフォルト値
    * @slot class <INPUT>タグに適用される
    * @slot placeholder <INPUT>タグに適用される
    * @slot maxlength <INPUT>タグに適用される
    * @slot options <INPUT>タグに適用される。class、placeholder、maxlengthは個別の分で上書きされる
    */


    // id、name：必須なので初期化しない
    //if(!isset($accesskey)) $accesskey = 'textbox';
    if(!isset($classes)) $classes = false;
    if(!isset($document)) $document = false;
    // クラスから渡された値()
    if(!isset($loading)) $loading = $_REQUEST;
    // デフォルト
    if(!isset($default)) $default = false;
    // オプション設定
    if(!isset($options)) $options = false;

    // 基本CSSクラス
    $__class_ = config('const.inputclasses.select');

    //-------------
    // オプション設定
    //-------------
    if(is_array($options)) {
        $thisoptions = $options;
    }else{
        $thisoptions = ['class'=>$__class_,'placeholder'=>'','maxlength'=>'120'];
    }
    // クラスが指定されている場合
    if(!array_key_exists('class',$thisoptions)) $thisoptions['class']=$__class_;
    if(isset($class)) $thisoptions['class']=$__class_;
    // プレースホルダが指定されている場合
    if(isset($placeholder)) $thisoptions['placeholder']=$placeholder;
    // 文字数が指定されている場合
    if(isset($maxlength)) $thisoptions['maxlength']=$maxlength;
    // ID
    if(!array_key_exists('id',$thisoptions)) $thisoptions['id']=$accesskey;
    if(isset($id)) $thisoptions['id']=$accesskey;
?>
@component('layouts.inputs._base')
    @slot('accesskey', $accesskey)
    @slot('classes', $classes)
    @slot('domcontent')
        <?php if(empty($domcontent)) { ?>
            {!! Form::select($accesskey, $pulldown, Funcs::rq($accesskey, $loading, $default), $thisoptions) !!}
        <?php }else{ ?>
            {{ $document }}
        <?php } ?>
    @endslot
@endcomponent
