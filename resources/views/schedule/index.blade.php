<?php
    // ページタイトル
    $title = '空き状況';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('schedule/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {{ Form::open(['method' => 'get', 'url' => 'schedule/search', 'class'=>'form-horizontal form-label-left']) }}
                    <div class="row pb-2 pl-1">
                        <div id='calendar'></div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection

{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
	<!-- fullcalendar -->
    <link href="{{ asset('asset/fullcalendar/css/fullcalendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/fullcalendar/css/style.css') }}" rel="stylesheet">
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
    <script src="{{ asset('asset/fullcalendar/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/fullcalendar/js/fullcalendar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/fullcalendar/js/gcal.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/fullcalendar/lang/ja.js') }}" type="text/javascript"></script>
    <script>
        // ページ読み込み時の処理
        $(document).ready(function () {
            // カレンダーの設定
            $('#calendar').fullCalendar({
                header: [{
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay,listMonth'
                }],
                eventSources : [{
                    googleCalendarApiKey: 'AIzaSyBwcSA3SG-m8gM8UjPx3rs9uZ1GLegzdYY',
                    googleCalendarId: 'japanese__ja@holiday.calendar.google.com',
                    rendering: 'background',
                    color:"#ffd0d0"
                }],
                events: function(start, end, timezone, callback) {
                   // ***** ここでカレンダーデータ取得JSを呼ぶ *****
                   setCalendarList(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), callback);
                },
                eventClick : function(event) {
                    $start = event.start.format('YYYY/MM/DD');
                    $time  = event.title;
                    window.location.href = 'reserve/search?reserve_date='+$start+'&reserve_time='+$time;
                    return false;
                },
//                height: 550,
                lang: "ja",
                selectable: true,
                selectHelper: true,
                editable: true,
                eventLimit: true,
            });
        });
    </script>
    <script>
        /**
         * 対象日付スケジュールデータセット処理
         * @param {type} startDate
         * @param {type} endDate
         * @param {type} callback
         * @returns {undefined}
         */
        function setCalendarList(startDate, endDate, callback) {
          $.ajax({
            type: 'get',
            dataType : "text",
            async: true,
            cache: false,
            url : "schedule/search?startDate="+startDate+"&endDate="+endDate
          })
          .then(
            function(data, startDate) {
              // JSONパース
              var obj = jQuery.parseJSON(data);
              var events = [];

              $.each(obj, function(index, value) {
                events.push({
                  // イベント情報をセット
                  title: value['title'],
                  description: value['description'],
                  start: value['start'],
                  end: value['end'],
                  color: value['color'],
                  textColor: value['textColor']
                });
              });

              // コールバック設定
              callback(events);
            }
          );

          return;
        }
    </script>
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@endsection
