<?php
    // ページタイトル
    $title = '開催日時初期データ作成';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

?>
@extends('layouts.app')

@section('content')
{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'exhibition/makesave', 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <!-- card-body -->
                <div class="card-body">
                    <div class="row pb-2 pl-1">
                        <div class="w80 mr-2 align-self-center text-nowrap">開催年</div>
                        <div class="w150 mr-2 align-self-center">
                            <div class="input-group date">
                                @component('layouts.inputs.yearpicker')
                                    @slot('accesskey', 'make_date')
                                    @slot('placeholder','例:2018')
                                    @slot('placeholder','例:2018')
                                    @slot('default','')
                                @endcomponent
                            </div>
                            <span style="color:#dc3545;">{{$errors->first('maake_date')}}</span>
                        </div>
                        <div class="row mt-3">
                            <div class="col"></div>
                            <div class="col-1">
                                {{ Form::submit('登録', ['id'=>'btn_save', 'class' => 'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / card-body -->
            </div>
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->
<script type="text/javascript">
$(function () {
    $('.maake_date').datepicker({
		format: 'yyyy/mm',
		language: 'ja',
		autoclose: true,
		minViewMode: 'months',
    });
});
</script>
<!-- /イベント -->

@endsection
