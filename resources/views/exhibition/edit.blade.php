<?php
    // ページタイトル
    $title = '開催日設定';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'OFF';

    // 開催無し:背景色
    $td_bcolor_exh = '#f0e68c';

    // コントローラ
//    $subnavs = [
//        ['text'=>'一覧','href'=>url('exhibition')],
//    ];

    // モーダルウィンドウ
    $modalid_customer = 'findcustomer';

    // ダイアログ
    $modalid_delete = 'dialogdelete';
?>
@extends('layouts.app')

@section('content')
{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'exhibition/save', 'class'=>'form-horizontal form-label-left']) }}
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-body"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">

                        {{-- 設定月 --}}
                        <div class="card-body">
                            <div class="row pb-2 pl-1">
                                <div class="w50 mr-2 align-self-center text-nowrap">開催月</div>
                                <div class="w150 mr-2 align-self-center">
                                    <div class="input-group date">
                                        @component('layouts.inputs.monthpicker')
                                            @slot('accesskey', 'exh_month')
                                            @slot('placeholder','例:2018')
                                        @endcomponent
                                    </div>
                                    <span style="color:#dc3545;">{{$errors->first('exh_month')}}</span>
                                </div>
                            </div>
                        </div>

                        {{-- 設定日時 --}}
                        <table id="main" class="table table-striped table-hover table-bordered">
                            <!--列名-->
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="6%">日にち</th>
                                    <th scope="col" width="1%"></th>
                                    {{-- 最大イベント数分列を作成 --}}
                                    @for($k=0; $k < $max_count; $k++)
                                        <th scope="col" width="6%">開催時間</th>
                                        <th scope="col" width="8%">最大プレート数</th>
                                    @endfor
                                </tr>
                            </thead>
                            <!-- /列名-->

                            <!--データ行-->
                            <tbody>
                                <?php
                                    //初期化
                                    // 取得データ数の格納
                                    $coloumn_num = $cnt_results;
                                    // 比較用：開催時間の格納
                                    $cond_date   = Funcs::rq('exh_date[0]', $data);
                                    // 列数のカウント(イベント開催時間)
                                    $col_cnt     = 0;
                                ?>
                                <!-- 初めの一行のみを作成 -->
                                <?php
                                    //該当行にある「開催時間」が開催無しの場合：着色する
                                    $rowCl    = '';
                                    $chk_ext_val = $data['chk_ext[1]'];
                                    if ($chk_ext_val==1) {
                                        $rowCl = 'background-color:'.$td_bcolor_exh;//「行」着色
                                    }
                                ?>
                                <tr style="{{$rowCl}}">
                                    <!-- 日にち -->
                                    <td class="text-center">
                                        {{$cond_date }}
                                        <?php
                                            setlocale(LC_ALL, 'ja_JP.UTF-8');
                                            $dt = new \Carbon\Carbon($cond_date);
                                            $week = $dt->formatLocalized('(%a)');
                                        ?>
                                        {{$week }}
                                    </td>
                                    <td>
                                        @component('layouts.inputs.check')
                                            @slot('accesskey','chk_ext[1]')
                                            @slot('loading', $data)
                                            @slot('options', ['class'=> 'form-group checkbox'])
                                        @endcomponent
                                    </td>
                                <!--  /初めの一行のみを作成 -->

                                {{-- DBより取得したデータをループ --}}
                                @for($i = 0; $i < $cnt_results; $i++)
                                    <?php
                                        $exh_date = Funcs::rq('exh_date['.$i.']', $data);
                                    ?>
                                    <!-- 開催日付が変わった場合 - 新規行の作成 -->
                                    @if ( $exh_date != $cond_date )
                                        {{-- START *************************************************************
                                            該当日(一行)が一番の最大イベント数分のデータ(列)があるか判別
                                            有る場合：空列は作らない　無い場合：空列を作成
                                             ************************************************************* --}}
                                        @if ( $max_count > $col_cnt)

                                            {{-- ▽▼▽▼▽▼ START 空列作成 ▽▼▽▼▽▼ --}}
                                            <!--空行(開催日未設定)の作成-->
                                            @for($col_cnt; $col_cnt < $max_count; $col_cnt++)
                                                <!-- 開催時間 -->
                                                <td id="td_exh_time[{{$coloumn_num}}]" class="text-left" style="background-color:{{$td_bcolor_exh}}">
                                                    @component('layouts.inputs.select')
                                                        @slot('accesskey','exh_time['.$coloumn_num.']')
                                                        @slot('loading', $data)
                                                        @slot('pulldown',$time_list)
                                                        @slot('options', ['class'=> $errors->has('exh_time['.$coloumn_num.']') ? 'form-control is-invalid ': 'form-control'])
                                                    @endcomponent
                                                    <span style="color:#dc3545;">{{$errors->first('exh_time.'.$coloumn_num)}}</span>
                                                </td>
                                                <!-- 最大プレート数 -->
                                                <td id="td_plate_maxnum[{{$coloumn_num}}]" class="text-left" style="background-color:{{$td_bcolor_exh}}">
                                                    @component('layouts.inputs.num')
                                                        @slot('accesskey', 'plate_maxnum['.$coloumn_num.']')
                                                        @slot('loading', $data)
                                                        @slot('default', 0)
                                                        @slot('options', ['readonly'=>'readonly', 'class'=> $errors->has('plate_maxnum['.$coloumn_num.']') ? 'form-control is-invalid ': 'form-control ja input-sm text-right'])
                                                    @endcomponent
                                                    <span style="color:#dc3545;">{{$errors->first('plate_maxnum.'.$coloumn_num)}}</span>
                                                </td>
                                                <!-- 開催ID -->
                                                {!! Form::hidden('exh_id['.$coloumn_num.']', Funcs::rq('exh_id['.$coloumn_num.']', $data)) !!}
                                                <!-- 開催日付 -->
                                                {!! Form::hidden('exh_date['.$coloumn_num.']', $cond_date) !!}

                                                <?php
                                                    $coloumn_num++;
                                                ?>
                                            @endfor
                                            {{-- △▲△▲△▲ END空列作成 △▲△▲△▲ --}}

                                        @endif
                                        <?php
                                            $col_cnt = 0;
                                        ?>
                                        </tr>
                                        {{-- END ************************************************************* --}}

                                        <!-- 初めの一行以外のヘッダーを作成 -->
                                        <?php
                                            //チェックボックスの命名:"chk_連番"
                                            $chk_name = substr($exh_date, -2);
                                            $chk_name = (int)$chk_name;

                                            //該当行にある「開催時間」が開催無しの場合：着色する
                                            $rowCl    = '';
                                            $chk_ext_val = $data['chk_ext['.$chk_name.']'];
                                            if ($chk_ext_val==1) {
                                                $rowCl = 'background-color:'.$td_bcolor_exh;//「行」着色
                                            }
                                        ?>
                                        <tr style="{{$rowCl}}">
                                            <!-- 日にち -->
                                            <?php
                                                $dt = new \Carbon\Carbon($exh_date);
                                                $week = $dt->formatLocalized('(%a)');
                                                $week_val = $dt->dayOfWeek;
                                                $week_color = '';
                                                if ($week_val==0) {         //日曜日
                                                    $week_color = 'color:#FF0000';
                                                }elseif ($week_val==6) {    //土曜日
                                                    $week_color = 'color:#1e90ff';
                                                }
                                            ?>
                                            <td class="text-center" style="{{$week_color}}">
                                                {{$exh_date }}
                                                {{$week }}
                                            </td>
                                            <td>
                                                @component('layouts.inputs.check')
                                                    @slot('accesskey','chk_ext['.$chk_name.']')
                                                    @slot('loading', $data)
                                                    @slot('options', ['class'=> 'form-group checkbox'])
                                                @endcomponent
                                            </td>
                                        <!-- /初めの一行以外のヘッダーを作成 -->
                                    @endif

                                    {{-- ▽▼▽▼▽▼ START DBより取得したデータのセット ▽▼▽▼▽▼ --}}
                                    <?php
                                        //開催日付が「開催無し」の場合：true
                                        $strRead = false;
                                        $bgCl    = '';
                                        $condD = Funcs::rq('exh_time['.$i.']', $data);

                                        if($condD==null){
                                            $strRead = true;    //開催日付;「開催無し」
                                            $bgCl    = 'background-color:'.$td_bcolor_exh;//「開催無し」着色
                                        }
                                    ?>
                                    <!-- 開催時間 -->
                                    <td id="td_exh_time[{{$i}}]" class="text-left" style="{{$bgCl}}">
                                        @component('layouts.inputs.select')
            								@slot('accesskey','exh_time['.$i.']')
                                            @slot('loading', $data)
                                            @slot('pulldown',$time_list)
                                            @slot('options', ['class'=> $errors->has('exh_time['.$i.']') ? 'form-control is-invalid ': 'form-control'])
                                        @endcomponent
                                        <span style="color:#dc3545;">{{$errors->first('exh_time.'.$i)}}</span>
                                    </td>
                                    <!-- 最大プレート数 -->
                                    <td id="td_plate_maxnum[{{$i}}]" class="text-left" style="{{$bgCl}}">
                                        @component('layouts.inputs.num')
                                            @slot('accesskey', 'plate_maxnum['.$i.']')
                                            @slot('loading', $data)
                                            @slot('options', ['readonly'=>$strRead, 'class'=> $errors->has('plate_maxnum['.$i.']') ? 'form-control is-invalid ': 'form-control ja input-sm text-right'])
                                        @endcomponent
                                        <span style="color:#dc3545;">{{$errors->first('plate_maxnum.'.$i)}}</span>
                                    </td>
                                    <!-- 開催ID -->
                                    {!! Form::hidden('exh_id['.$i.']', Funcs::rq('exh_id['.$i.']', $data)) !!}
                                    <!-- 開催日付 -->
                                    {!! Form::hidden('exh_date['.$i.']', Funcs::rq('exh_date['.$i.']', $data)) !!}
                                    {{-- △▲△▲△▲ 　DBより取得したデータのセット △▲△▲△▲ --}}

                                    <?php $cond_date = $exh_date;   //比較用:開催日付の格納 ?>
                                    <?php $col_cnt++;               //比較用:何列目かをカウント ?>
                                @endfor

                                <!-- 最終行の空列のみ作成 -->
                                {{-- ▽▼▽▼▽▼ START 空列作成 ▽▼▽▼▽▼ --}}
                                @if ( $max_count > $col_cnt)
                                    @for($col_cnt; $col_cnt < $max_count; $col_cnt++)
                                            <!-- 開催時間 -->
                                            <td id="td_exh_time[{{$coloumn_num}}]" class="text-left" style="background-color:{{$td_bcolor_exh}}">
                                                @component('layouts.inputs.select')
                                                    @slot('accesskey','exh_time['.$coloumn_num.']')
                                                    @slot('loading', $data)
                                                    @slot('pulldown',$time_list)
                                                    @slot('options', ['class'=> $errors->has('exh_time['.$coloumn_num.']') ? 'form-control is-invalid ': 'form-control'])
                                                @endcomponent
                                                <span style="color:#dc3545;">{{$errors->first('exh_time.'.$coloumn_num)}}</span>
                                            </td>
                                            <!-- 最大プレート数 -->
                                            <td id="td_plate_maxnum[{{$coloumn_num}}]" class="text-left" style="background-color:{{$td_bcolor_exh}}">
                                                @component('layouts.inputs.num')
                                                    @slot('accesskey', 'plate_maxnum['.$coloumn_num.']')
                                                    @slot('loading', $data)
                                                    @slot('default', 0)
                                                    @slot('options', ['readonly'=>'readonly', 'class'=> $errors->has('plate_maxnum['.$coloumn_num.']') ? 'form-control is-invalid ': 'form-control ja input-sm text-right'])
                                                @endcomponent
                                                <span style="color:#dc3545;">{{$errors->first('plate_maxnum.'.$coloumn_num)}}</span>
                                            </td>
                                            <!-- 開催ID -->
                                            {!! Form::hidden('exh_id['.$coloumn_num.']', Funcs::rq('exh_id['.$coloumn_num.']', $data)) !!}
                                            <!-- 開催日付 -->
                                            {!! Form::hidden('exh_date['.$coloumn_num.']', $cond_date) !!}
                                        </tr>
                                    @endfor
                                @endif
                                {{-- △▲△▲△▲ END空列作成 △▲△▲△▲ --}}
                                <!-- /最終行の空列の作成 -->
                            </tbody>
                            <!-- /データ行-->
                        </table>

                        <!-- 最大イベント数 -->
                        {!! Form::hidden('max_count', $max_count) !!}
                        <!-- 登録更新データ件数 -->
                        {!! Form::hidden('cnt_results', $cnt_results) !!}

                        <table class="table text-center">
                            <tr>
                                <td>
                                    <div class="claim_btn_box">
                                        {{ Form::submit('登録', ['class' => 'btn btn-primary btn-sm btn_list_mstyle1', 'data-toggle'=>'tooltip', 'title'=>'入力データを保存する']) }}
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div><br></div>
    </div>
</div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->
<script type="text/javascript">
$(function(){
    // 開催月の変更
    $('#exh_month').blur(function() {
        var name = $(this).val();
        window.location.href = 'edit?exh_month='+$(this).val();
    });

    // 開催時間
    $('select[name^=exh_time]').on('change', function(){
        var name = $(this).attr('name');
        var val = $(this).val();
        var rowid = pickindex(name);

        //開催時間が「開催無し」以外の場合：入力可
        if (val) {
            //最大プレート数入力可
            $('input[name="plate_maxnum['+rowid+']"]').attr('readonly',false);
            //開催時間,最大プレート数:背景色デフォルト
            document.getElementById('td_exh_time['+rowid+']').style.backgroundColor = '';
            document.getElementById('td_plate_maxnum['+rowid+']').style.backgroundColor = '';
        //開催時間が「開催無し」の場合：入力不可
        }else{
            //最大プレート数入力不可
            $('input[name="plate_maxnum['+rowid+']"]').attr('readonly',true);
            //開催時間,最大プレート数:入力不可色
            document.getElementById('td_exh_time['+rowid+']').style.backgroundColor = '{{$td_bcolor_exh}}';
            document.getElementById('td_plate_maxnum['+rowid+']').style.backgroundColor = '{{$td_bcolor_exh}}';
        }
    });

    function pickindex(name){
        var array = name.match(/[0-9]+\.?[0-9]*/g);

        return parseInt(array[0]);
    }

    // チェックボックスをチェックしたら発動
    $('input[name^=chk_ext]').change(function() {
        var name = $(this).attr('name');
        var val = $(this).val();
        var rowid = pickindex(name);

        var prop = $(this).prop('checked');
        if (prop) {
            //最大プレート数：入力不可色
            $(this).parent().parent().parent().css('background-color','{{$td_bcolor_exh}}');
            //最大プレート数：入力不可
            $(this).parent().parent().parent().find('input[name^="plate_maxnum"]').attr('readonly',true);
            //開催時間：「開催無し」
            $(this).parent().parent().parent().find('select[name^="exh_time"]').val('');
        }else{
            //最大プレート数：入力可色
            $(this).parent().parent().parent().css('background-color','');
            //最大プレート数：入力可
            $(this).parent().parent().parent().find('input[name^="plate_maxnum"]').attr('readonly',false);
            //開催時間：「09:00」
            $(this).parent().parent().parent().find('select[name^="exh_time"]').val('09:00');
        }
    });

});

</script>
<!-- /イベント -->

@endsection
