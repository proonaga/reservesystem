<?php
    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';
?>
@extends('layouts.app')

@section('content')

<!-- container -->
<div class="navi_sub">

    <!-- navi_sub_left -->
    <div class="navi_sub_left">
        <label class="clear"></label>
    </div><!-- navi_sub_left -->

    <div class="clear"></div>
</div><!-- container -->

<!-- container_page -->
<div class="container_page">

    <div class="title_box">
        <div class="title_left">
            <h3>{{ config('app.name', 'Laravel') }}</h3>
        </div>
        <div class="title_right">
        </div>
        <div class="clear"></div>
        <hr>
    </div>

</div>
<!-- /container_page -->


@endsection