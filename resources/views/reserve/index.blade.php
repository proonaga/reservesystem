<?php
    // ページタイトル
    $title = '予約一覧';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'新規登録','href'=>url('reserve/edit')],
    ];

    // モーダルウィンドウ
    $modalid_clear = 'catalog_alert_clear';
?>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {{ Form::open(['method' => 'get', 'url' => 'reserve/search', 'class'=>'form-horizontal form-label-left']) }}
                    <div class="row pb-2 pl-1">
                        <div class="w40 mx-3 align-self-center text-nowrap">予約者名</div>
                        <div class="w600 mx-3 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'represent_name')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1 align-reserves-start">
                        <div class="w40 mx-3 align-self-center text-nowrap">開催日</div>
                        <div class="w160 mx-3 align-self-center">
                            <div class="input-group date">
                                @component('layouts.inputs.datepicker')
                                    @slot('accesskey', 'reserve_date')
                                    @slot('default','')
                                @endcomponent
                            </div>
                        </div>
                        <div class="w140 mx-3 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'reserve_time')
                                @slot('pulldown', Config::get('const.time_list'))
                                @slot('default', '')
                            @endcomponent
                        </div>
                    </div><!-- .row -->
                    <div class="row pb-2 pl-1 align-reserves-start">
                        <div class="col">
                            <div class="row justify-content-end">
                                <div class="mx-3">
                                    <button type="submit" class="btn btn-primary">検索</button>
                                </div>
                                <div class="mx-3">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$modalid_clear}}">クリア</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- .row -->
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="card">
            <!--div class="card-header"></div-->
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
                <div class="number_style">
                    @if ( $results->Total() )
                        該当件数 <span>{{ $results->Total() }}</span> 件のうち <span>{{ $results->firstItem() }} ～ {{ $results->lastItem() }}</span> 件目を表示
                    @else
                        該当件数 <span>{{ $results->Total() }}</span> 件
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="6%">開催日</th>
                                    <th scope="col" width="6%">時間</th>
                                    <th scope="col" width="20%">予約者名</th>
                                    <th scope="col" width="20%">グループ名</th>
                                    <th scope="col" width="10%">国籍</th>
                                    <th scope="col" width="6%">参加人数</th>
                                    <th scope="col" width="10%">プレート</th>
                                    <th scope="col" width="6%">金額</th>
                                    <th scope="col" width="13%">決済</th>
                                    <th scope="col">編集</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $row)
                                <tr>
                                    <td class="text-center">{{ date_format( date_create($row->reserve_date), 'Y/m/d') }}</td>
                                    <td class="text-left">{{ date_format( date_create($row->reserve_time), 'H:i') }}</td>
                                    <td class="text-left">{{ $row->represent_name }}</td>
                                    <td class="text-left">{{ $row->group_name }}</td>
                                    <td class="text-left">{{ Config::get('const.nationality_list')[$row->nationality] }}</td>
                                    <td class="text-right">{{ number_format( $row->number_adult + $row->number_child ) }}</td>
                                    <?php
                                        $query = DB::table('reserve_detail');
                                        $query->join('item', 'reserve_detail.item_id', '=', 'item.id');
                                        $query->where('reserve_detail.reserve_id','=',$row->id);
                                        $list = $query->get();
                                    ?>
                                    <td class="text-left">
                                        @foreach($list as $row_plate)
                                            <?php
                                                $fontcolor="";
                                                if ($row_plate->plate_num > 10) $fontcolor ='color:#FF0000';
                                            ?>
                                            <span style="{{$fontcolor}}">{{ $row_plate->plate_num }}</span> /
                                            ({{ $row_plate->item_name }})<br>
                                        @endforeach
                                    </td>
                                    <td class="text-right   ">{{ number_format( $row->price ) }}</td>
                                    <td class="text-center">{{ Config::get('const.settlement_type')[$row->settlement_type] }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="{{url('reserve/edit')}}/{{$row->id}}">編集</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- ページャ -->
            			<nav  class="nav_style_height">
            			    {{ $results->links() }}
            			</nav>
            			<!-- /ページャ -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')

@component('layouts.modal')
    @slot('modalid', $modalid_clear)
    @slot('modaltitle', 'クリア')
    @slot('modalcontent')
        クリアします
    @endslot
    @slot('modalfooter')
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doclear();return false;">OK</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
        <script type="text/javascript">
            function doclear(){
                window.location.href='{{ url('reserve/search') }}';
            }
        </script>
    @endslot
@endcomponent

@endsection
