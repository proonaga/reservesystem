<?php
    // ページタイトル
    $title = '予約登録';

    // ページ上部「エラーメッセージ表示ON/OFF」
    $err_message_flg = 'ON';

    // コントローラ
    $subnavs = [
        ['text'=>'一覧','href'=>url('reserve')],
    ];

    // モーダルウィンドウ
    $modalid_customer = 'findcustomer';

    // ダイアログ
    $modalid_delete = 'dialogdelete';
?>
@extends('layouts.app')

@section('content')
{{ Form::open(['id'=>'input-form', 'method' => 'post', 'url' => 'reserve/save', 'class'=>'form-horizontal form-label-left']) }}
    <div class="row justify-content-center w1024">
        <div class="col-12">
            <div class="card">
                <!-- card-body -->
                <div class="card-body">
                    @if( !empty( Funcs::rq('id', $data['results']) ) )
                        <div class="row pb-2 pl-1">
                            <div class="w80 mr-2 align-self-center text-nowrap">ID</div>
                            <div class="w600 mr-2 align-self-center">
                                {{ Funcs::rq('id', $data['results']) }}
                            </div>
                        </div><!--/.row-->
                    @endif
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">予約者名</div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'reserve_name')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）御菓子太郎')
                                @slot('options', ['class'=> $errors->has("reserve_name") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('reserve_name')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">開催日</div>
                        <div class="w150 mr-2 align-self-center">
                            <div class="input-group date">
                                @component('layouts.inputs.datepicker')
                                    @slot('accesskey', 'reserve_date')
                                    @slot('loading', $data['results'])
                                    @slot('options', ['class'=> $errors->has('reserve_date') ? 'form-control is-invalid ': 'form-control'])
                                @endcomponent
                            </div>
                            <span style="color:#dc3545;">{{$errors->first('reserve_date')}}</span>
                        </div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'reserve_time')
                                @slot('pulldown', Config::get('const.time_list'))
                                @slot('default', '')
                                @slot('options', ['class'=> $errors->has('reserve_time') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('reserve_time')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">人数</div>
                        <div class="w100 mr-2 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'number_adult')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）2')
                                @slot('options', ['class'=> $errors->has("number_adult") ? 'form-control is-invalid ': 'form-control ja input-sm text-right'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('number_adult')}}</span>
                        </div>
                        <div class="w40 mr-2 align-self-center text-nowrap">大人</div>
                        <div class="w100 mr-2 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'number_child')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）3')
                                @slot('options', ['class'=> $errors->has("number_child") ? 'form-control is-invalid ': 'form-control ja input-sm text-right'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('number_child')}}</span>
                        </div>
                        <div class="w20 mr-2 align-self-center text-nowrap">子供</div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">プレート</div>
                        <?php $k=1; ?>
                        @foreach($item_lists as $row)
                            <div class="w200 mr-2 align-self-center">
                                @component('layouts.inputs.select')
                                    @slot('accesskey', 'item_plate['.$k.']')
                                    @slot('pulldown', $row)
                                    @slot('options', ['class'=> $errors->has('item_plate['.$k.']') ? 'form-control is-invalid ': 'form-control'])
                                @endcomponent
                                <span style="color:#dc3545;">{{$errors->first('item_plate['.$k.']')}}</span>
                            </div>
                            <?php $k++; ?>
                        @endforeach
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">金額</div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.num')
                                @slot('accesskey', 'price')
                                @slot('loading', $data['results'])
                                @slot('options', ['class'=> $errors->has("price") ? 'form-control is-invalid ': 'form-control ja input-sm text-right'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('price')}}</span>
                        </div>
                        <div class="w20 mr-2 align-self-center text-nowrap">円</div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">グループ名</div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'group_name')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）グループA')
                                @slot('options', ['class'=> $errors->has("group_name") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('group_name')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">TEL</div>
                        <div class="w200 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'tel')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）090-1245-6789')
                                @slot('options', ['class'=> $errors->has("tel") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('tel')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">メールアドレス</div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 'email')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）yamada@softbank.jp')
                                @slot('options', ['class'=> $errors->has("email") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('email')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">国籍</div>
                        <div class="w150 mr-2 align-self-center">
                            @component('layouts.inputs.select')
                                @slot('accesskey', 'nationality')
                                @slot('pulldown', Config::get('const.nationality_list'))
                                @slot('default', '')
                                @slot('options', ['class'=> $errors->has('nationality') ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('nationality')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">添乗員名</div>
                        <div class="w400 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 't_name')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）搭乗花子')
                                @slot('options', ['class'=> $errors->has("t_name") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('t_name')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">旅行会社TEL</div>
                        <div class="w200 mr-2 align-self-center">
                            @component('layouts.inputs.text')
                                @slot('accesskey', 't_telno')
                                @slot('loading', $data['results'])
                                @slot('placeholder', '例）090-1245-6789')
                                @slot('options', ['class'=> $errors->has("t_telno") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('t_telno')}}</span>
                        </div>
                    </div><!--/.row-->
                    <div class="row pb-2 pl-1">
                        <div class="w100 mr-2 align-self-center text-nowrap">コメント</div>
                        <div class="w400 mr-2 align-self-center text-nowrap">
                            @component('layouts.inputs.textarea')
                                @slot('accesskey', 'comment')
                                @slot('loading', $data['results'])
                                @slot('options', ['rows' => 2, 'class'=> $errors->has("comment") ? 'form-control is-invalid ': 'form-control'])
                            @endcomponent
                            <span style="color:#dc3545;">{{$errors->first('comment')}}</span>
                        </div>
                    </div><!--/.row-->
                </div>
                <!-- / card-body -->

                {{ Form::hidden('id', Funcs::rq('id',$data['results'])) }}
            </div>
        </div>
    </div>
    @csrf
{{ Form::close() }}
@endsection


{{-- 追加<HEADER>タグ内 --}}
@section('addheader')
@endsection

{{-- 追加文末部分 --}}
@section('postdocument')
<!-- イベント -->
<script type="text/javascript">
$(function(){

    // プレート数プルダウン
    // 「金額」の算出
    $('select[name^=item_plate]').on('change', function(){
        // プレートプルダウン数を格納
        $cnt = {{ count($item_lists) }}
        alert('{{ count($item_lists) }}');

//        var name = $(this).attr('name');
//        var val = $(this).val();
//        var rowid = pickindex(name);
//
//        //開催時間が「開催無し」以外の場合：入力可
//        if (val) {
//            //最大プレート数入力可
//            $('input[name="plate_maxnum['+rowid+']"]').attr('readonly',false);
//            //開催時間,最大プレート数:背景色デフォルト
//            document.getElementById('td_exh_time['+rowid+']').style.backgroundColor = '';
//            document.getElementById('td_plate_maxnum['+rowid+']').style.backgroundColor = '';
//        }
    });

});

</script>
<!-- /イベント -->

<!-- 削除 -->
@if( !empty( Funcs::rq('id', $data['results']) ) )
    @component('layouts.modal')
        @slot('modalid', $modalid_delete)
        @slot('modaltitle', '削除')
        @slot('modalcontent')
            削除してよろしいですか？
        @endslot
        @slot('modalfooter')
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="dodelete();return false;">OK</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
            <script type="text/javascript">
                function dodelete(){
                    $('#form-delete').submit();
                }
            </script>
            <form id="form-delete" method="post" action="{{url('reserve/destroy').'/'.Funcs::rq('id',$data['results'])}}">
                {!! Form::hidden('id', Funcs::rq('id',$data['results'])) !!}
                @csrf
            </form>
        @endslot
    @endcomponent
@endif

@endsection
