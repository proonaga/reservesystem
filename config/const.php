<?php
//DBで定義されている区分などは「defines.php」へ
return [

    //
    'default_maxrowsize' => env('DEFAULT_MAXROWSIZE', 20),
    'default_popuprowsize' => env('DEFAULT_POPUPROWSIZE', 10),

    // INPUTタグのクラス
    // @see resources/views/layouts/inputs/*.blade.php
    'inputclasses' => [
        'text'          => 'form-control ja input-sm',
        'check'         => 'form-control ja input-sm transform: scale(1.5);',
        'select'        => 'form-control ja input-sm',
        'textarea'      => 'form-control ja input-sm',
        'num'           => 'form-control ja input-sm text-right',
        'datepicker'    => 'form-control ja input-sm datepicker',
        'monthpicker'   => 'form-control ja input-sm monthpicker',
        'yearpicker'    => 'form-control ja input-sm yearpicker',
    ],

    'default_maxplate' => env('DEFAULT_MAXPLATE', 60),

    // 決済方法
    'settlement_type' => [
        '1' => 'キャッシュ',    //キャッシュ
        '2' => 'クレジット',    //クレジット
        '3' => 'その他',        //その他
    ],

    // 国籍
    'nationality_list' => [
        '1' => '日本',
        '2' => '台湾',
        '3' => '韓国',
        '4' => 'アメリカ',
    ],

    // 時刻
    'time_list' => [
        '09:00' => '09:00',
        '10:00' => '10:00',
        '11:00' => '11:00',
        '12:00' => '12:00',
        '13:00' => '13:00',
        '14:00' => '14:00',
        '15:00' => '15:00',
        '16:00' => '16:00',
        '17:00' => '17:00',
        '18:00' => '18:00',
        '19:00' => '19:00',
        '20:00' => '20:00',
        '21:00' => '21:00',
        '22:00' => '22:00',
        '23:00' => '23:00',
        '00:00' => '00:00',
        '01:00' => '01:00',
        '02:00' => '02:00',
        '03:00' => '03:00',
        '04:00' => '04:00',
        '05:00' => '05:00',
        '06:00' => '06:00',
        '07:00' => '07:00',
        '08:00' => '08:00',
    ],

    //都道府県
    'pref' => array(
        "01" => "北海道",
        "02" => "青森県",
        "03" => "岩手県",
        "04" => "宮城県",
        "05" => "秋田県",
        "06" => "山形県",
        "07" => "福島県",
        "08" => "茨城県",
        "09" => "栃木県",
        "10" => "群馬県",
        "11" => "埼玉県",
        "12" => "千葉県",
        "13" => "東京都",
        "14" => "神奈川",
        "15" => "新潟県",
        "16" => "富山県",
        "17" => "石川県",
        "18" => "福井県",
        "19" => "山梨県",
        "20" => "長野県",
        "21" => "岐阜県",
        "22" => "静岡県",
        "23" => "愛知県",
        "24" => "三重県",
        "25" => "滋賀県",
        "26" => "京都府",
        "27" => "大阪府",
        "28" => "兵庫県",
        "29" => "奈良県",
        "30" => "和歌山県",
        "31" => "鳥取県",
        "32" => "島根県",
        "33" => "岡山県",
        "34" => "広島県",
        "35" => "山口県",
        "36" => "徳島県",
        "37" => "香川県",
        "38" => "愛媛県",
        "39" => "高知県",
        "40" => "福岡県",
        "41" => "佐賀県",
        "42" => "長崎県",
        "43" => "熊本県",
        "44" => "大分県",
        "45" => "宮崎県",
        "46" => "鹿児島県",
        "47" => "沖縄県",
    ),

    //都道府県
    'pref_key_nm' => array(
        ""         => "",
        "北海道"   => "北海道",
        "青森県"   => "青森県",
        "岩手県"   => "岩手県",
        "宮城県"   => "宮城県",
        "秋田県"   => "秋田県",
        "山形県"   => "山形県",
        "福島県"   => "福島県",
        "茨城県"   => "茨城県",
        "栃木県"   => "栃木県",
        "群馬県"   => "群馬県",
        "埼玉県"   => "埼玉県",
        "千葉県"   => "千葉県",
        "東京都"   => "東京都",
        "神奈川"   => "神奈川",
        "新潟県"   => "新潟県",
        "富山県"   => "富山県",
        "石川県"   => "石川県",
        "福井県"   => "福井県",
        "山梨県"   => "山梨県",
        "長野県"   => "長野県",
        "岐阜県"   => "岐阜県",
        "静岡県"   => "静岡県",
        "愛知県"   => "愛知県",
        "三重県"   => "三重県",
        "滋賀県"   => "滋賀県",
        "京都府"   => "京都府",
        "大阪府"   => "大阪府",
        "兵庫県"   => "兵庫県",
        "奈良県"   => "奈良県",
        "和歌山県" => "和歌山県",
        "鳥取県"   => "鳥取県",
        "島根県"   => "島根県",
        "岡山県"   => "岡山県",
        "広島県"   => "広島県",
        "山口県"   => "山口県",
        "徳島県"   => "徳島県",
        "香川県"   => "香川県",
        "愛媛県"   => "愛媛県",
        "高知県"   => "高知県",
        "福岡県"   => "福岡県",
        "佐賀県"   => "佐賀県",
        "長崎県"   => "長崎県",
        "熊本県"   => "熊本県",
        "大分県"   => "大分県",
        "宮崎県"   => "宮崎県",
        "鹿児島県" => "鹿児島県",
        "沖縄県"   => "沖縄県",
    ),
];
