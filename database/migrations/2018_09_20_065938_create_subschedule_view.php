<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscheduleView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( "CREATE VIEW sub_schedule AS
                        select
                          reserve.reserve_date
                          , reserve.reserve_time
                          , sum(detail.plate_num) as plate_num
                        from
                          reserve
                          inner join reserve_detail detail
                            on reserve.id = detail.reserve_id
                        group by
                            reserve.reserve_date
                          , reserve.reserve_time
                    " );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement( 'DROP VIEW IF EXISTS sub_schedule' );
    }
}
