<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->string('item_name',40)->nullable()->comment('商品名');
            $table->decimal('item_fee', 10, 2)->default(0.00)->comment('料金');
            $table->string('add_user_name', 20)->comment('登録者');
            $table->string('upd_user_name', 20)->comment('更新者');
            $table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE item COMMENT '商品テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
