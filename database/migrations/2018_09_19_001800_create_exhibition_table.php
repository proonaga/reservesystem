<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibition', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->datetime('exh_date')->comment('開催日');
            $table->time('exh_time')->nullable()->comment('開催時間');
            $table->integer('plate_maxnum')->nullable()->default(0)->comment('最大プレート数');
            $table->string('add_user_name', 20)->comment('登録者');
            $table->string('upd_user_name', 20)->comment('更新者');
            $table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE exhibition COMMENT '開催日設定テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibition');
    }
}
