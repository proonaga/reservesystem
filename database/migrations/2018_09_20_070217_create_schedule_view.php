<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( "CREATE VIEW schedule AS
                        select
                          ex1.exh_date
                          , ex1.exh_time
                          , ex1.plate_maxnum
                          , ifnull(sub_schedule.plate_num, 0) as number
                        from
                          exhibition ex1
                          left join sub_schedule
                            on ex1.exh_date = sub_schedule.reserve_date
                            and ex1.exh_time = sub_schedule.reserve_time
                    " );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement( 'DROP VIEW IF EXISTS schedule' );
    }
}
