<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReserveDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserve_detail', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->integer('reserve_id')->comment('予約ID');
            $table->integer('item_id')->comment('商品ID');
            $table->integer('plate_num')->comment('プレート数');
            $table->string('add_user_name', 20)->comment('登録者');
            $table->string('upd_user_name', 20)->comment('更新者');
            $table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE reserve_detail COMMENT '予約詳細テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserve_detail');
    }
}
