<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReserveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserve', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->datetime('reserve_date')->comment('予約日');
            $table->time('reserve_time')->comment('予約時間');
            $table->integer('number_adult')->default(0)->comment('大人人数');
            $table->integer('number_child')->default(0)->comment('子供人数');
            $table->string('represent_name', 60)->comment('代表者名');
            $table->decimal('price', 10, 2)->default(0.00)->comment('金額');
            $table->string('group_name', 120)->nullable()->comment('グループ名');
            $table->string('tel', 20)->nullable()->comment('電話番号');
            $table->string('email')->nullable()->comment('EMAIL');
            $table->string('nationality', 3)->nullable()->comment('国籍');
            $table->string('settlement_type', 2)->comment('決済方法');
            $table->text('comment')->nullable()->comment('コメント');
            $table->string('add_user_name', 20)->comment('登録者');
            $table->string('upd_user_name', 20)->comment('更新者');
            $table->timestamps();
            $table->softDeletes();
        });
        // ALTER 文を実行しテーブルにコメントを設定
        DB::statement("ALTER TABLE reserve COMMENT '予約テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserve');
    }
}
